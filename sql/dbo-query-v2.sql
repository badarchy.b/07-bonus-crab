-- JIRA TICKET
IF OBJECT_ID('tempdb..#MonthlyResults') IS NOT NULL
    DROP TABLE #MonthlyResults;
DECLARE @start DATE = '2022-01-01';
-- Initialize the ending month as December 31, 2023
DECLARE @end DATE = '2023-12-31';
DECLARE @currentFinish DATE;

-- Temporary table to store aggregated results
CREATE TABLE #MonthlyResults (
    Month_ DATE,
    Business NVARCHAR(50),
    License NVARCHAR(50),
    AffiliateProgram NVARCHAR(50),
    Brand NVARCHAR(50),
    Traffic NVARCHAR(50),
    Country NVARCHAR(50),
    Reg INT,
    Ftd INT,
    Ftd_casino INT,
    Ftd_sport INT,
    Actives INT,
    Actives_casino INT,
    Actives_sport INT,
    Retained INT,
    Retained_casino INT,
    Retained_sport INT,
    Per_Retained DECIMAL(20, 2),
    Deposit DECIMAL(20, 2),
    Net_cash DECIMAL(20, 2),
    GGRCasinoTotal_EUR DECIMAL(20, 2),
    GGRSportTotal_EUR DECIMAL(20, 2),
    NGR DECIMAL(20, 2),
    ARPU DECIMAL(20, 2),
    ARPU_FTD DECIMAL(20, 2),
    ARPU_old DECIMAL(20, 2),
    Deposit_FTD DECIMAL(20, 2),
    Net_Cash_FTD DECIMAL(20, 2),
    Deposit_old DECIMAL(20, 2),
    Net_cash_old DECIMAL(20, 2),
    NGR_FTD DECIMAL(20, 2),
    NGR_old DECIMAL(20, 2),
    Active_VIP INT,
    Retained_VIP INT,
    Deposit_VIP DECIMAL(20, 2),
    Net_cash_VIP DECIMAL(20, 2),
    NGR_VIP DECIMAL(20, 2),
    TO_Casino DECIMAL(20, 2),
    TO_Sport DECIMAL(20, 2),
    ActiveDays INT,
    Retained_PrM INT,
    Retained_Bef_PrM INT,
    Depositors INT,
    DepRet_PrM INT,
    DepRet_Bef_PrM INT,
    OldDepositors INT
);

-- Loop through each month
WHILE @start <= @end
BEGIN
    -- Adjust @currentFinish for the current iteration to be the end of the current @start month
    SET @currentFinish = EOMONTH(@start);

    INSERT INTO #MonthlyResults
    SELECT
        k.mm AS Month_,
        br.business,
        k.License,
        k.affiliate_program,
        k.brand,
        k.traffic,
        CASE
            WHEN k.country IN ('DE','IT','FI','SE','PL','NO','AT','IE','HU','CA','CZ','BR','CH','IN','NZ','JP','TH','AU','EE','TR','CL','PE','SK','SI','KZ','AZ') THEN k.country
            WHEN k.country_name IN ('ROW1','ROW2','ROW3') THEN k.country_name
            WHEN k.country='DK' AND k.License = 'DGA' THEN k.country
            WHEN k.country IN ('KW','BH','QA','AE','SA','LB','JO','OM','IQ','YE','EG') THEN 'ME'
            ELSE 'ROW'
        END AS country,
        SUM(reg) AS reg,
        SUM(ftd) AS ftd,
        SUM(ftd)-SUM(ftd_sport) AS ftd_casino,
        SUM(ftd_sport) AS ftd_sport,
        SUM(Actives) AS Actives,
        SUM(Actives)-SUM(Actives_sport) AS Actives_casino,
        SUM(Actives_sport) AS Actives_sport,
        SUM(Actives) - SUM(ftd) AS Retained,
        SUM(Actives)-SUM(Actives_sport)-(SUM(ftd)-SUM(ftd_sport)) AS Retained_casino,
        SUM(Actives_sport)-SUM(ftd_sport) AS Retained_sport,
        ISNULL ( (SUM(Actives) - SUM(ftd)) / (NULLIF(1.00*SUM(Actives),0)), 0) AS per_Retained,
        SUM(deposit) AS deposit,
        SUM(deposit) - SUM(withdraw) AS Net_cash,
        SUM(GGRCasino_EUR) AS GGRCasinoTotal_EUR,
        SUM(GGRSport_EUR) AS GGRSportTotal_EUR,
        SUM(NGR) AS NGR,
        ISNULL ( (SUM(NGR)) / (NULLIF(1.00*SUM(Actives),0)), 0) AS ARPU,
        ISNULL ( (SUM(NGR_FTD)) / (NULLIF(1.00*SUM(ftd),0)), 0) AS ARPU_FTD,
        ISNULL ( (SUM(NGR)-SUM(NGR_FTD)) / (NULLIF(1.00*(SUM(Actives) - SUM(ftd)),0)), 0) AS ARPU_old,
        ISNULL(SUM(deposit_FTD),0) AS deposit_FTD,
        ISNULL(SUM(deposit_FTD) - SUM(withdraw_FTD),0) AS Net_Cash_FTD,
        ISNULL(SUM(deposit) - SUM(deposit_FTD),0) AS deposit_old,
        ISNULL(SUM(deposit) - SUM(deposit_FTD) - SUM(withdraw) + SUM(withdraw_FTD),0) AS net_cash_old,
        SUM(NGR_FTD) AS NGR_FTD,
        SUM(NGR) - SUM(NGR_FTD) AS NGR_old,
        SUM(active_VIP) AS active_VIP,
        SUM(active_VIP)-SUM(FTD_VIP) AS retained_VIP,
        SUM(dep_VIP) AS deposit_VIP,
        SUM(net_VIP) AS net_cash_VIP,
        SUM(NGR_VIP) AS NGR_VIP,
        SUM(TO_Casino) AS TO_Casino,
        SUM(TO_Sport) AS TO_Sport,
        SUM(ActiveDays) AS ActiveDays,
        SUM(Retained_PrM) AS Retained_PrM,
        SUM(Retained_Bef_PrM) AS Retained_Bef_PrM,
        SUM(depositors) AS depositors,
        SUM(DepRet_PrM) AS DepRet_PrM,
        SUM(DepRet_Bef_PrM) AS DepRet_Bef_PrM,
        SUM(old_depositors) AS old_depositors
    FROM (
        SELECT
            C.brand,
            BR.License,
            BR.affiliate_program,
            C.domain,
            C.country,
            C.country_name,
            CASE WHEN C.traffic='affiliate' THEN 'affiliate' ELSE 'media' END AS traffic,
            EOMONTH(C.created) AS mm,
            COUNT(*) AS reg,
            0 AS ftd,
            0 AS ftd_sport,
            0 AS deposit,
            0 AS depositors,
            0 AS deposit_FTD,
            0 AS withdraw,
            0 AS withdraw_FTD,
            0 AS Actives,
            0 AS Actives_sport,
            0 AS GGRCasino_EUR,
            0 AS bonus_GGRCasino_EUR,
            0 AS GGRSport_EUR,
            0 AS bonus_GGRSport_EUR,
            0 AS DepositCount,
            0 AS NGR,
            0 AS NGR_FTD,
            0 AS active_VIP,
            0 AS FTD_VIP,
            0 AS dep_VIP,
            0 AS net_VIP,
            0 AS NGR_VIP,
            0 AS TO_Casino,
            0 AS TO_Sport,
            0 AS ActiveDays,
            0 AS Retained_PrM,
            0 AS Retained_Bef_PrM,
            0 AS DepRet_PrM,
            0 AS DepRet_Bef_PrM,
            0 AS old_depositors
        FROM [dbo].[Dim_Customers] C WITH(NOLOCK)
        JOIN [dbo].[Dim_Brands] BR WITH(NOLOCK) ON BR.entity_id=C.entity_id AND BR.domain=C.domain
        WHERE C.type IN ('casino','sport') AND CAST(C.created AS DATE) BETWEEN @start AND @currentFinish
        GROUP BY
            C.brand,
            BR.License,
            BR.affiliate_program,
            C.domain,
            C.country,
            C.country_name,
            CASE WHEN C.traffic='affiliate' THEN 'affiliate' ELSE 'media' END,
            EOMONTH(C.created)
        UNION
        SELECT
            C.brand,
            BR.License,
            BR.affiliate_program,
            C.domain,
            C.country,
            C.country_name,
            CASE WHEN C.traffic='affiliate' THEN 'affiliate' ELSE 'media' END AS traffic,
            EOMONTH(C.first_deposit_date) AS mm,
            0 AS reg,
            COUNT(*) AS ftd,
            COUNT(CASE WHEN C.product_preference IN ('mix','pure_sport') THEN 1 ELSE NULL END) AS ftd_sport,
            0 AS deposit,
            0 AS depositors,
            0 AS deposit_FTD,
            0 AS withdraw,
            0 AS withdraw_FTD,
            0 AS Actives,
            0 AS Actives_sport,
            0 AS GGRCasino_EUR,
            0 AS bonus_GGRCasino_EUR,
            0 AS GGRSport_EUR,
            0 AS bonus_GGRSport_EUR,
            0 AS DepositCount,
            0 AS NGR,
            0 AS NGR_FTD,
            0 AS active_VIP,
            0 AS FTD_VIP,
            0 AS dep_VIP,
            0 AS net_VIP,
            0 AS NGR_VIP,
            0 AS TO_Casino,
            0 AS TO_Sport,
            0 AS ActiveDays,
            0 AS Retained_PrM,
            0 AS Retained_Bef_PrM,
            0 AS DepRet_PrM,
            0 AS DepRet_Bef_PrM,
            0 AS old_depositors
        FROM [dbo].[Dim_Customers] C WITH(NOLOCK)
        JOIN [dbo].[Dim_Brands] BR WITH(NOLOCK) ON BR.entity_id=C.entity_id AND BR.domain=C.domain
        WHERE C.type IN ('casino','sport') AND C.first_deposit_date BETWEEN @start AND @currentFinish
        GROUP BY
            C.brand,
            BR.License,
            BR.affiliate_program,
            C.domain,
            C.country,
            C.country_name,
            CASE WHEN C.traffic='affiliate' THEN 'affiliate' ELSE 'media' END,
            EOMONTH(C.first_deposit_date)
        UNION
        SELECT
            C.brand,
            BR.License,
            BR.affiliate_program,
            C.domain,
            C.country,
            C.country_name,
            CASE WHEN C.traffic='affiliate' THEN 'affiliate' ELSE 'media' END AS traffic,
            EOMONTH(ReportingDate) AS mm,
            0 AS reg,
            0 AS ftd,
            0 AS ftd_sport,
            SUM(DepositAmount_EUR) AS deposit,
            COUNT(DISTINCT CASE WHEN CC.DepositCount>0 THEN CC.account_id ELSE NULL END) AS depositors,
            SUM(CASE WHEN EOMONTH(C.first_deposit_date) = EOMONTH(ReportingDate) THEN CC.DepositAmount_EUR ELSE 0 END) AS deposit_FTD,
            SUM(CC.WithdrawAmount_EUR) AS withdraw,
            SUM(CASE WHEN EOMONTH(C.first_deposit_date) = EOMONTH(ReportingDate) THEN CC.WithdrawAmount_EUR ELSE 0 END) AS withdraw_FTD,
            COUNT(DISTINCT CASE WHEN CC.TurnoverCasino_EUR+CC.TurnoverSport_EUR>0 THEN CC.account_id ELSE NULL END) AS Actives,
            COUNT(DISTINCT CASE WHEN CC.TurnoverSport_EUR>0 THEN CC.account_id ELSE NULL END) AS Actives_sport,
            SUM(GGRCasino_EUR) AS GGRCasino_EUR,
            SUM(bonus_GGRCasino_EUR) AS bonus_GGRCasino_EUR,
            SUM(GGRSport_EUR) AS GGRSport_EUR,
            SUM(bonus_GGRSport_EUR) AS bonus_GGRSport_EUR,
            SUM(DepositCount) AS DepositCount,
            SUM(NGR) AS NGR,
            SUM(CASE WHEN EOMONTH(C.first_deposit_date) = EOMONTH(ReportingDate) THEN NGR ELSE 0 END) AS NGR_FTD,
            COUNT(DISTINCT CASE WHEN CC.TurnoverCasino_EUR+CC.TurnoverSport_EUR>0 AND ISNULL(L.real_level_vip,1)>2 THEN CC.account_id ELSE NULL END) AS active_VIP,
            COUNT(DISTINCT CASE WHEN EOMONTH(C.first_deposit_date) = EOMONTH(ReportingDate) AND ISNULL(L.real_level_vip,1)>2 THEN CC.account_id ELSE NULL END) AS FTD_VIP,
            SUM(CASE WHEN ISNULL(L.real_level_vip,1)>2 THEN CC.DepositAmount_EUR ELSE 0 END) AS dep_VIP,
            SUM(CASE WHEN ISNULL(L.real_level_vip,1)>2 THEN CC.DepositAmount_EUR-CC.WithdrawAmount_EUR ELSE 0 END) AS net_VIP,
            SUM(CASE WHEN ISNULL(L.real_level_vip,1)>2 THEN CC.NGR ELSE 0 END) AS NGR_VIP,
            SUM(CC.TurnoverCasino_EUR+CC.bonus_TurnoverCasino_EUR) AS TO_Casino,
            SUM(CC.TurnoverSport_EUR+CC.bonus_TurnoverSport_EUR) AS TO_Sport,
            COUNT(CASE WHEN CC.TurnoverCasino_EUR+CC.TurnoverSport_EUR>0 THEN CONCAT(C.account_id,C.domain) ELSE NULL END) AS ActiveDays,
            COUNT(DISTINCT CASE WHEN CC.TurnoverCasino_EUR+CC.TurnoverSport_EUR>0 AND EOMONTH(C.first_deposit_date) = EOMONTH(ReportingDate,-1) THEN CC.account_id ELSE NULL END) AS Retained_PrM,
            COUNT(DISTINCT CASE WHEN CC.TurnoverCasino_EUR+CC.TurnoverSport_EUR>0 AND EOMONTH(C.first_deposit_date) < EOMONTH(ReportingDate,-1) THEN CC.account_id ELSE NULL END) AS Retained_Bef_PrM,
            COUNT(DISTINCT CASE WHEN CC.DepositCount>0 AND EOMONTH(C.first_deposit_date) = EOMONTH(ReportingDate,-1) THEN CC.account_id ELSE NULL END) AS DepRet_PrM,
            COUNT(DISTINCT CASE WHEN CC.DepositCount>0 AND EOMONTH(C.first_deposit_date) < EOMONTH(ReportingDate,-1) THEN CC.account_id ELSE NULL END) AS DepRet_Bef_PrM,
            COUNT(DISTINCT CASE WHEN CC.DepositCount>0 AND EOMONTH(D.LastDep) = EOMONTH(ReportingDate,-1) AND EOMONTH(C.first_deposit_date) < EOMONTH(ReportingDate,-1) THEN CC.account_id ELSE NULL END) AS old_depositors
        FROM Fact_CustomerDailyActivity CC WITH(NOLOCK)
        JOIN [dbo].[Dim_Customers] C WITH(NOLOCK) ON C.account_id=CC.account_id AND CC.domain=C.domain
        JOIN [dbo].[Dim_Brands] BR WITH(NOLOCK) ON BR.entity_id=C.entity_id AND BR.domain=C.domain
        OUTER APPLY (
            SELECT TOP 1
                CASE
                    WHEN (L.level<2 OR L.level IS NULL) THEN 1
                    WHEN (L.level<3 AND L.level>=2) THEN 2
                    WHEN (L.level<4 AND L.level>=3) THEN 3
                    WHEN (L.level<5 AND L.level>=4) THEN 4
                    WHEN L.level=5 THEN 5
                END AS real_level_vip
            FROM Fact_LoyaltyLevel L WITH(NOLOCK) WHERE L.account_id=CC.account_id AND L.date <= EOMONTH(ReportingDate) AND L.domain=CC.domain
            ORDER BY L.date DESC
        ) L
        OUTER APPLY (
            SELECT MAX(A.ReportingDate) AS LastDep
            FROM Fact_CustomerDailyActivity A WITH(NOLOCK)
            WHERE A.account_id=CC.account_id AND A.domain=CC.domain AND EOMONTH(A.ReportingDate)<EOMONTH(CC.ReportingDate) AND A.DepositCount>0
        ) D
        WHERE C.type IN ('casino','sport') AND CC.ReportingDate BETWEEN @start AND @currentFinish
        GROUP BY
            C.brand,
            BR.License,
            BR.affiliate_program,
            C.domain,
            C.country,
            C.country_name,
            CASE WHEN C.traffic='affiliate' THEN 'affiliate' ELSE 'media' END,
            EOMONTH(ReportingDate)
    ) k
    JOIN Dim_Brands br WITH(NOLOCK) ON br.name = k.brand AND br.domain = k.domain
--     WHERE br.business IN ('b2c') AND k.traffic IN ('affiliate', 'media')
    WHERE br.business IN ('b2c','b2b')
    GROUP BY
        k.mm,
        br.business,
        k.License,
        k.affiliate_program,
        k.brand,
        k.traffic,
        CASE
            WHEN k.country IN ('DE','IT','FI','SE','PL','NO','AT','IE','HU','CA','CZ','BR','CH','IN','NZ','JP','TH','AU','EE','TR','CL','PE','SK','SI','KZ','AZ') THEN k.country
            WHEN k.country_name IN ('ROW1','ROW2','ROW3') THEN k.country_name
            WHEN k.country='DK' AND k.License = 'DGA' THEN k.country
            WHEN k.country IN ('KW','BH','QA','AE','SA','LB','JO','OM','IQ','YE','EG') THEN 'ME'
            ELSE 'ROW'
        END;

    -- Move the @start to the next month
    SET @start = DATEADD(MONTH, 1, @start);
END;

-- Select the aggregated results
SELECT * FROM #MonthlyResults;
